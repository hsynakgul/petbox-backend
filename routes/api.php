<?php

use Domains\Auth\Controllers\AuthController;
use Domains\Breed\Controllers\BreedController;
use Domains\Category\Controllers\CategoryController;
use Domains\Chat\Controllers\ChatController;
use Domains\Chat\Controllers\ChatMessageController;
use Domains\File\Controllers\FileController;
use Domains\Post\Controllers\PostController;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::post("auth/register", [AuthController::class, 'register']);
Route::post("auth/login", [AuthController::class, 'login']);

Route::middleware('auth:sanctum')->group(function () {
    Route::get("auth/me", [AuthController::class, 'me']);
    Route::post("auth/update", [AuthController::class, 'update']);
    Route::post("auth/suspend", [AuthController::class, 'suspend']);
    Route::delete("auth/destroy", [AuthController::class, 'destroy']);
    Route::post("auth/pusher", [AuthController::class, 'pusherAuth']);

    Route::delete('categories/batch-destroy', [CategoryController::class, 'batchDestroy'])->name("categories.batch-destroy");
    Route::post('categories/batch-restore', [CategoryController::class, 'batchRestore'])->name("categories.batch-restore");
    Route::apiResource("categories", CategoryController::class)
        ->except(["destroy"]);

    Route::delete('breeds/batch-destroy', [BreedController::class, 'batchDestroy'])->name("breeds.batch-destroy");
    Route::apiResource("breeds", BreedController::class)
        ->except(["destroy"]);

    Route::delete('posts/batch-destroy', [PostController::class, 'batchDestroy'])->name("posts.batch-destroy");
    Route::get('posts/favorites', [PostController::class, 'favorites'])->name("posts.favorites");
    Route::post('posts/favorite/{post}', [PostController::class, 'favorite'])->name("posts.favorite");
    Route::post('posts/status/{post}', [PostController::class, 'status'])->name("posts.status");
    Route::apiResource("posts", PostController::class);

    Route::apiResource("chats", ChatController::class)
        ->only(["index", "store", "destroy"]);
    Route::apiResource("posts", PostController::class);

    Route::apiResource("chats/messages", ChatMessageController::class)
        ->only(["index", "store", "destroy"]);

    Route::apiResource("files", FileController::class)->only(["store", "destroy"]);
});

Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});
