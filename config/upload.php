<?php
return [
    "allowExtensions" => ["jpg", "jpeg", "HEIC", "png", "pdf", "svg", "mp4"],
    "imageExtensions" => ["jpg", "jpeg", "png"],
    "maxUploadSize" => [
        "image" => 10, // MB
        "otherFile" => 64 // MB
    ],
    "imageConfig" => array(
        "strict_sizes" => "0",
        "aoe" => true, //AllowOutputEnlargement
        "zc" => null,
        "ar" => true, //AutoRotate
        "quality" => "60",
        "maxwidth" => "1920",
        "maxheight" => "1080",
        "forcewatermark" => false,
        "watermark" => true,
        "watermark_url" => "/img/watermark.png",
        "watermark_width" => "75",
        "watermark_height" => "75",
        "width" => null,
        "height" => null,
        "filters" => []
    )
];
