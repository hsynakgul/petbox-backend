<?php


namespace App;


class Application extends \Illuminate\Foundation\Application
{
    /**
     * Overrides the path to the application "app" directory.
     *
     * @return string
     */
    public function path($path = ''): string
    {
        $appPath = $this->appPath ?: $this->basePath.DIRECTORY_SEPARATOR.'src/App';

        return $appPath.($path ? DIRECTORY_SEPARATOR.$path : $path);
    }
}
