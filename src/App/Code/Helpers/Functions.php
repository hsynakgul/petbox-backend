<?php

use Carbon\Carbon;

if (!function_exists('dateFormat')) {
    /**
     * Get the path to a versioned Mix file.
     *
     * @param string|Carbon $date
     * @param string $format
     *
     * @return string
     */
    function dateFormat($date, $format = ''): string
    {
        if (is_string($date)) $date = Carbon::createFromFormat("Y-m-d H:i:s", $date);
        $format = match ($format) {
            'date-long' => 'D MMMM YYYY, dddd',
        };
        return $date->isoFormat($format);
    }
}

if (!function_exists('ucwords_tr')) {
    /**
     * Get the path to a versioned Mix file.
     *
     * @param string $str
     *
     * @return string
     */
    function ucwords_tr($str = '')
    {
        $result = '';
        $words = explode(" ", $str);

        foreach ($words as $word) {
            $words_length = strlen($word);
            $first_letter = mb_substr($word, 0, 1, 'UTF-8');
            switch ($first_letter) {
                case 'Ç':
                case 'ç':
                    $first_letter = 'Ç';
                    break;
                case 'Ğ':
                case 'ğ':
                    $first_letter = 'Ğ';
                    break;
                case 'I':
                case 'ı':
                    $first_letter = 'I';
                    break;
                case 'İ':
                case 'i':
                    $first_letter = 'İ';
                    break;
                case 'Ö':
                case 'ö':
                    $first_letter = 'Ö';
                    break;
                case 'Ş':
                case 'ş':
                    $first_letter = 'Ş';
                    break;
                case 'Ü':
                case 'ü':
                    $first_letter = 'Ü';
                    break;
                default:
                    $first_letter = strtoupper($first_letter);
                    break;
            }

            $other_letters = mb_substr($word, 1, $words_length, 'UTF-8');
            $result .= $first_letter . strtolower_tr($other_letters) . ' ';
        }
        return trim(preg_replace('!\s+!', ' ', $result));
    }
}

if (!function_exists('strtolower_tr')) {
    /**
     * Get the path to a versioned Mix file.
     *
     * @param string $str
     *
     * @return string
     */
    function strtolower_tr($str = '')
    {
        $str = str_replace('Ç', 'ç', $str);
        $str = str_replace('Ğ', 'ğ', $str);
        $str = str_replace('I', 'ı', $str);
        $str = str_replace('İ', 'i', $str);
        $str = str_replace('Ö', 'ö', $str);
        $str = str_replace('Ş', 'ş', $str);
        $str = str_replace('Ü', 'ü', $str);
        $str = strtolower($str);
        return $str;
    }
}

if (!function_exists('strtoupper_tr')) {
    /**
     * Get the path to a versioned Mix file.
     *
     * @param string $str
     *
     * @return string
     */
    function strtoupper_tr($str = '')
    {
        $str = str_replace('ç', 'Ç', $str);
        $str = str_replace('ğ', 'Ğ', $str);
        $str = str_replace('ı', 'I', $str);
        $str = str_replace('i', 'İ', $str);
        $str = str_replace('ö', 'Ö', $str);
        $str = str_replace('ş', 'Ş', $str);
        $str = str_replace('ü', 'Ü', $str);
        $str = strtoupper($str);
        return $str;
    }
}

if (!function_exists('getFirstCharsOfWords')) {
    /**
     * Get the path to a versioned Mix file.
     *
     * @param string $str
     *
     * @return string
     */
    function getFirstCharsOfWords($str = '')
    {
        $acronym = "";
        foreach (preg_split("/\s+/", $str) as $word)
            $acronym .= mb_substr($word, 0, 1, 'utf-8');
        return strtoupper_tr($acronym);
    }
}
