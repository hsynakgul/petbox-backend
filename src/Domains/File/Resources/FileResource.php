<?php


namespace Domains\File\Resources;


use Illuminate\Http\Resources\Json\JsonResource;

class FileResource extends JsonResource
{
    public function toArray($request)
    {
        return [
            "id" => $this->id,
            "name" => $this->name,
            "avatar" => $this->avatar,
            "thumb" => $this->thumb,
            "small" => $this->small,
            "large" => $this->large
        ];
    }
}
