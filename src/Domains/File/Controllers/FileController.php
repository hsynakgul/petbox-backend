<?php


namespace Domains\File\Controllers;


use App\Http\Controllers\Controller;
use Domains\File\Actions\CreateWithFilterAction;
use Domains\File\Actions\UploadFileAction;
use Domains\File\Models\File;
use Domains\File\Requests\FileStoreRequest;
use Domains\File\Resources\FileResource;

class FileController extends Controller
{
    public function store(FileStoreRequest $request, UploadFileAction $uploadFileAction)
    {
        return new FileResource(
            $uploadFileAction($request->file("file"), $request)
        );
    }

    public function destroy(File $file)
    {
        $storagePath = storage_path("uploads/photos/" . $file->folder . "/" . $file->name);
        if (file_exists($storagePath)) unlink($storagePath);

        $publicPath = public_path("uploads/photos/" . $file->folder . "/{parameter}/" . $file->name);
        $parameters = array_keys(CreateWithFilterAction::$parameters);
        foreach ($parameters as $parameter) {
            $path = str_replace("{parameter}", $parameter, $publicPath);
            if (file_exists($path)) unlink($path);
        }

        $file->delete();

        return response()->noContent();
    }

    public function createWithFilter(CreateWithFilterAction $createWithFilterAction, $folder, $parameters, $file)
    {
        return $createWithFilterAction($folder, $parameters, $file);
    }
}
