<?php


namespace Domains\File\Models;


use Illuminate\Database\Eloquent\Model;

class File extends Model
{
    protected $guarded = ['id'];

    protected $casts = [
        "created_at" => "string"
    ];

    public function getFileURL(string $type): string
    {
        return env("APP_URL") . "/uploads/photos/" . $this->folder . "/" . $type . "/" . $this->name;
    }

    public function getAvatarAttribute(): string
    {
        return $this->getFileURL("avatar");
    }

    public function getThumbAttribute(): string
    {
        return $this->getFileURL("thumb");
    }

    public function getSmallAttribute(): string
    {
        return $this->getFileURL("small");
    }

    public function getLargeAttribute(): string
    {
        return $this->getFileURL("large");
    }
}
