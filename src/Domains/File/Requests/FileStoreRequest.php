<?php


namespace Domains\File\Requests;


use Illuminate\Foundation\Http\FormRequest;

class FileStoreRequest extends FormRequest
{
    public function rules(): array
    {
        return [
            "name" => "required|max:255",
            "file" => "required|file"
        ];
    }
}
