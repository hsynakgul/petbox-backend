<?php


namespace Domains\File\Actions;


use Domains\File\Models\File;
use Illuminate\Http\Request;
use Illuminate\Http\UploadedFile;
use Illuminate\Support\Str;

class UploadFileAction
{
    private array $config;

    public function __construct()
    {
        $this->config = config('upload');
    }

    public function __invoke(UploadedFile $uploadedFile, Request $request): File
    {
        $extension = strtolower(substr($uploadedFile->getClientOriginalExtension(), 0, 4));
        $allowExtensions = $this->config['allowExtensions'] ?? [];
        if (!in_array($extension, $allowExtensions)) {
            abort(500, "Sadece belirtilen (" . implode(', ', $allowExtensions) . ") uzantılı dosyaları yükleyebilirsiniz!");
        }

        $imageExtensions = $this->config['imageExtensions'] ?? [];
        $maxUploadSize = in_array($extension, $imageExtensions) ?
            ($this->config['maxUploadSize']['image'] ?? 3)
            : $this->config['maxUploadSize']['otherFile'] ?? 3;

        if ($uploadedFile->getSize() > 1024 * 1024 * $maxUploadSize)
            abort(500, $maxUploadSize . "MB Upload file size exceeded.");

        $file = new File([
            "folder_id" => $request->input("folder_id", 0),
            "user_id" => auth()->id(),
            "title" => pathinfo($uploadedFile->getClientOriginalName(), PATHINFO_FILENAME),
            "folder" => date("ym"),
            "size" => $uploadedFile->getSize(),
            "extension" => $extension
        ]);

        if (in_array($extension, $imageExtensions)) {
            list($width, $height) = getimagesize($uploadedFile);
            $file->fill([
                "width" =>$width,
                "height" => $height,
                "ratio" =>$this->getRatio($width, $height)
            ]);
        }

        $file->save();

        $file->name = Str::slug($file->title) . '-' . $file->id . '.' . $file->extension;
        $file->save();

        $uploadedFile->move(storage_path("uploads/photos/". $file->folder), $file->name);

        return $file;
    }

    private function getRatio($width, $height): string
    {
        $rate = number_format($width / $height, 1);
        return match ($rate) {
            1 => '1:1',
            1.3 => '4:3',
            1.5 => '3:2',
            1.7, 1.8 => '16:9',
            default => $width . ':' . $height,
        };
    }
}
