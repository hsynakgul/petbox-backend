<?php


namespace Domains\File\Actions;


use Symfony\Component\HttpKernel\Exception\BadRequestHttpException;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;
use Intervention\Image\Facades\Image as InterventionImage;

class CreateWithFilterAction
{
    private $thumb;
    private $file;
    private $filename;
    private $dirPath;
    private $model;
    private array $filters = array();
    private $config;
    private array $allowedExtension = ["png", "jpg", "jpeg", "gif"];
    public static array $parameters = [
        "avatar" => "w_128,c_fitin,q_90",
        "thumb" => "w_256,c_fitin,q_90",
        "small" => "w_512,c_fitin,q_90",
        "large" => "w_1024,c_fitin,q_90"
    ];

    public function __construct()
    {
        $this->config = config('upload.imageConfig');
    }

    public function __invoke($folder, $parameters, $file)
    {
        if (!array_key_exists($parameters, self::$parameters)) throw new BadRequestHttpException('Filtre Utgun Değil');
//        if (!preg_match("/^([c|e|h|w|q]_[a-z0-9]+,?)+$/", $parameters)) throw new BadRequestHttpException('Filtre Utgun Değil');

        $source_image = storage_path() . "/uploads/photos/" . $folder . "/" . $file;
        $new_path = public_path() . "/uploads/photos/" . $folder . "/" . $parameters . "/";

        if (!is_file($source_image)) throw new NotFoundHttpException( "Resim dosyası bulunamadı!");

        return $this->create($source_image, self::$parameters[$parameters])->save($new_path)->show();
    }

    public function create($file, $parameters = null)
    {
        $this->file = $file;
        $this->filename = basename($this->file);
        $this->dirPath = rtrim($this->file, $this->filename);
        $this->thumb = InterventionImage::make($this->file);

        if (!is_null($parameters)) $this->setParameters($parameters);

        return $this;
    }

    public function save($savePath = null, $filename = null): self
    {
        if (!is_dir($savePath)) {
            mkdir($savePath, 0777, true);
        }

        $this->thumb->save(($savePath ? $savePath : $this->dirPath) . ($filename ? $filename : $this->filename), $this->config["quality"]);


        return $this;
    }

    public function show()
    {
        return $this->thumb->response();
    }

    private function setParameters($properties): self
    {

        /*
        * Usage
        * c_fill     : Crop zoom image
        * c_fit      : Crop resize image
        * c_fitin    : Crop resize zoom image
        * e_sharpen  : Use this for small images
        * e_sepia_80 : Sepia effect change 80 as you desire
        * h_80    : Height change 80 as you desire
        * w_80    : Width change 80 as you desire
        * q_80     : Quality change 80 as you desire
        */

        $pattern = '/([c|e|q|h|w])_([a-z0-9]+)_?([a-z0-9]+)?/m';
        preg_match_all($pattern, $properties, $matches);

        if (count($matches[0]) > 0)
            foreach ($matches[0] as $k => $v) {
                if ($matches[1][$k] == "w") {
                    if (!empty($matches[2][$k])) $this->config["width"] = (string)$matches[2][$k];
                }
                if ($matches[1][$k] == "h") {
                    if (!empty($matches[2][$k])) $this->config["height"] = (string)$matches[2][$k];
                }
                if ($matches[1][$k] == "q") {
                    if (!empty($matches[2][$k])) $this->config["quality"] = (string)$matches[2][$k];
                }
                if ($matches[1][$k] == "c") {
                    if ($matches[2][$k] == "fill") $this->config["zc"] = "fill";
                    elseif ($matches[2][$k] == "fit") $this->config["zc"] = "fit";
                    elseif ($matches[2][$k] == "fitin") $this->config["zc"] = "fitin";
                    else throw new \Exception("Yanlış crop türü");
                }
                if ($matches[1][$k] == "e") {
                    if ($matches[2][$k] == "wtmrk") $this->config["forcewatermark"] = true;
                    elseif ($matches[2][$k] == "nowtmrk") $this->config["watermark"] = false;
                    elseif ($matches[2][$k] == "sharpen") $this->thumb->sharpen(15); //$this->config["filters"][] = "usm|80|0.8|3";
                    elseif ($matches[2][$k] == "gray") $this->thumb->greyscale(); //$this->config["filters"][] = "gray";
                    elseif ($matches[2][$k] == "sepia") $this->thumb->greyscale()->colorize(20, 0, -20);//{$def = 20; if(!empty($matches[3][$k])) $def = $matches[3][$k]; $this->config["filters"][] = "sep|".$def;}
                    elseif ($matches[2][$k] == "blur") $this->thumb->blur(15);//{$def = 13; if(!empty($matches[3][$k])) $def = $matches[3][$k]; $this->config["filters"][] = "blur|".$def;}
                    elseif ($matches[2][$k] == "bright") $this->thumb->brightness(15);//{$def = 13; if(!empty($matches[3][$k])) $def = $matches[3][$k]; $this->config["filters"][] = "blur|".$def;}
                    else throw new \Exception("Yanlış efekt");
                }

            }
//        if($this->config["watermark"] and ((int)$this->config["width"] > (int)$this->config["watermark_width"] and (int)$this->config["height"] > (int)$this->config["watermark_height"]) ) $this->config["filters"][] = "wmi|" . public_path() . "/img/logo.png|BR";

        if ($this->config["zc"]) {
            $this->{$this->config["zc"]}();
        }

        $this->addWatermark();

        $this->orientate();

        return $this;
    }

    private function orientate()
    {
        if ($this->config["ar"]) {
            $this->thumb->orientate();
        }
    }

    private function addWatermark()
    {
        if (is_file(public_path() . $this->config["watermark_url"]) and ($this->config["forcewatermark"] or
                ($this->config["watermark"] and ((int)$this->config["width"] > (int)$this->config["watermark_width"] or (int)$this->config["height"] > (int)$this->config["watermark_height"]))
            )) {
            $watermark = InterventionImage::make(public_path() . $this->config["watermark_url"])->resize(round((int)$this->config["width"]), null, function ($constraint) {
                $constraint->aspectRatio();
            })->opacity(20);
            $this->thumb->insert($watermark, 'center');
        }
    }

    private function fill()
    {
        if ($this->config["width"] != null and $this->config["height"] != null) {
            $this->thumb->fit($this->config["width"], $this->config["height"], function ($constraint) {
                //$constraint->upsize();
            });
        }
    }

    private function fit()
    {
        if ($this->config["width"] != null and $this->config["height"] != null) {
            $this->thumb->resize($this->config["width"], $this->config["height"], function ($data) {
                $data->aspectRatio();
            });
        } elseif ($this->config["width"] != null) {
            $this->thumb->widen($this->config["width"]);
        } elseif ($this->config["height"] != null) {
            $this->thumb->heighten($this->config["height"]);
        }
    }

    private function fitin()
    {
        $this->thumb->resize($this->config["width"], $this->config["height"], function ($data) {
            $data->aspectRatio();
        });

        if ($this->config["width"] != null and $this->config["height"] != null) {
            $this->thumb->resizeCanvas($this->config["width"], $this->config["height"], 'center', false, 'ffffff');
        }
    }
}
