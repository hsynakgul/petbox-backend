<?php


namespace Domains\Breed\Models;


use Domains\Category\Observers\CategoryObserver;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Breed extends Model
{
    protected $guarded = ['id'];

    public $timestamps = false;

    public function getTitleAttribute($locale = null): string
    {
        if (!$locale) $locale = config('app.locale');
        $fallbackLocale = config('app.fallback_locale');

        if (array_key_exists("title", $this->attributes)) return $this->attributes["title"];
        if (array_key_exists("title_" . $locale, $this->attributes)) return $this->attributes["title_" . $locale];
        if (array_key_exists("title_" . $fallbackLocale, $this->attributes)) return $this->attributes["title_" . $fallbackLocale];
        return '';
    }
}
