<?php


namespace Domains\Breed\Resources;


use Illuminate\Http\Resources\Json\JsonResource;

class BreedResource extends JsonResource
{
    public function toArray($request)
    {
        return [
            "id" => $this->id,
            "title" => $this->title,
            "category_id" => $this->category_id,
        ];
    }
}
