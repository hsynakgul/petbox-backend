<?php


namespace Domains\Breed\Requests;


use Illuminate\Foundation\Http\FormRequest;

class BreedStoreRequest extends FormRequest
{
    public function rules(): array
    {
        return [
            "title_tr" => "required|max:255",
            "title_en" => "nullable|max:255",
            "status" => "nullable|integer|in:0,1",
            "category_id" => "required|exists:categories,id",
        ];
    }
}
