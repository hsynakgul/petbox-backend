<?php


namespace Domains\Breed\Controllers;


use App\Http\Controllers\Controller;
use App\Http\Requests\GlobalBatchDestroyRequest;
use Domains\Breed\Actions\DeleteBreedAction;
use Domains\Breed\DataTransferObjects\BreedData;
use Domains\Breed\Models\Breed;
use Domains\Breed\Requests\BreedStoreRequest;
use Domains\Breed\Resources\BreedResource;

class BreedController extends Controller
{
    public function index()
    {
        return BreedResource::collection(Breed::where("status", 1)->get());
    }

    public function show(Breed $breed)
    {
        return new BreedResource($breed);
    }

    public function store(BreedStoreRequest $request)
    {
        $breedData = BreedData::fromRequest($request);
        return new BreedResource(
            Breed::create($breedData->toArray())
        );
    }

    public function update(BreedStoreRequest $request, Breed $breed)
    {
        $breedData = BreedData::fromRequest($request);
        $breed->fill($breedData->toArray())
            ->save();
        return new BreedResource(
            $breed->refresh()
        );
    }

    public function destroy(Breed $breed)
    {
        return $breed->delete();
    }

    public function batchDestroy(GlobalBatchDestroyRequest $request, DeleteBreedAction $deleteBreedAction)
    {
        list($success, $failed) = $deleteBreedAction($request->get("id", []));
        return response()->json(compact('success', 'failed'));
    }
}
