<?php


namespace Domains\Breed\Actions;


use Domains\Breed\Models\Breed;

class DeleteBreedAction
{
    private array $success;
    private array $failed;

    public function __construct()
    {
        $this->success = [];
        $this->failed = [];
    }

    public function __invoke(array $ids): array
    {
        foreach ($ids as $id) $this->eachItem($id);
        return [$this->success, $this->failed];
    }

    private function eachItem($id)
    {
        $deleted = Breed::where("id", $id)->delete();
        if ($deleted === 0) {
            $this->failed[] = [
                "id" => $id,
                "message" => "Hayvan türü silinemedi! Böyle bir tür olmayabilir."
            ];
            return;
        }

        $this->success[] = $id;
    }
}
