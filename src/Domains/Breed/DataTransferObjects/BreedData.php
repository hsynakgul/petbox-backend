<?php


namespace Domains\Breed\DataTransferObjects;


use Domains\Breed\Requests\BreedStoreRequest;
use Illuminate\Support\Collection;
use Spatie\DataTransferObject\DataTransferObject;

class BreedData extends DataTransferObject
{
    public ?string $title_tr;
    public ?string $title_en;
    public ?int $category_id;
    public ?int $status;

    public static function fromRequest(BreedStoreRequest $request): self
    {
        return self::fromCollection(collect($request));
    }

    public static function fromCollection(Collection $request): self
    {
        return new self([
            'title_tr' => $request->get("title_tr", ""),
            'title_en' => $request->get("title_en", ""),
            'status' => (int)$request->get("status", 1),
            'category_id' => (int)$request->get("category_id", 0),
        ]);
    }
}
