<?php


namespace Domains\User\DataTransferObjects;


use Domains\User\Models\User;
use Domains\User\Requests\UserStoreRequest;
use Illuminate\Support\Collection;
use Spatie\DataTransferObject\DataTransferObject;

class UserData extends DataTransferObject
{
    public ?int $type;
    public ?string $name;
    public ?string $email;
    public ?string $phone;
    public ?int $gender;
    public ?int $status;
    public ?string $password;

    public static function fromRequest(UserStoreRequest $request): self
    {
        return self::fromCollection(collect($request));
    }

    public static function fromCollection(Collection $request): self
    {
        $instance = new self([
            "type" => (int)$request->get("type", User::TYPE_USER),
            "name" => $request->get("name", ""),
            "email" => $request->get("email", ""),
            "phone" => $request->get("gsm", ""),
            "gender" => (int)$request->get("gender", 0),
            "status" => (int)$request->get("status", 1),
        ]);

        if ($request->get("password")) {
            $instance->password = \Hash::make($request->get("password"));
        } else {
            $instance = $instance->except("password");
        }

        return $instance;
    }
}
