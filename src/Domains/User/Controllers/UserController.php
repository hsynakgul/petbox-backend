<?php


namespace Domains\User\Controllers;


use App\Http\Controllers\Controller;
use App\Http\Requests\GlobalBatchDestroyRequest;
use Domains\User\Actions\DeleteUserAction;
use Domains\User\Actions\RestoreUserAction;
use Domains\User\DataTransferObjects\UserData;
use Domains\User\Models\User;
use Domains\User\Requests\UserStoreRequest;
use Domains\User\Requests\UserUpdateRequest;
use Domains\User\Resources\UserResource;

class UserController extends Controller
{
    public function show(User $user)
    {
        return new UserResource($user);
    }

    public function store(
        UserStoreRequest $request
    )
    {
        $userData = UserData::fromRequest($request);
        return new UserResource(
            User::create($userData->toArray())
        );
    }

    public function update(
        UserUpdateRequest $request,
        User $user
    )
    {
        $userData = UserData::fromRequest($request);
        $user->fill($userData->except("site")->toArray())
            ->save();
        return new UserResource(
            $user->refresh()
        );
    }

    public function batchDestroy(
        GlobalBatchDestroyRequest $request,
        DeleteUserAction $deleteUserAction
    )
    {
        list($success, $failed) = $deleteUserAction($request->get("id", []));
        return response()->json(compact('success', 'failed'));
    }

    public function batchRestore(
        GlobalBatchDestroyRequest $request,
        RestoreUserAction $restoreUserAction
    )
    {
        list($success, $failed) = $restoreUserAction($request->get("id", []));
        return response()->json(compact('success', 'failed'));
    }
}
