<?php


namespace Domains\User\Requests;


use Domains\User\Models\User;
use Illuminate\Foundation\Http\FormRequest;

class UserStoreRequest extends FormRequest
{
    public function rules(): array
    {
        return [
            'name' => 'required|max:255',
            'email' => 'required|email|max:255|unique:users,email',
            'password' => 'required|min:6|max:16',
            'type' => 'required|in:' . implode(',', User::ENUM["types"]),
            'gender' => 'in:0,1,2',
            'send_email' => 'in:0,1',
            'send_sms' => 'in:0,1',
            'send_notification' => 'in:0,1',
        ];
    }
}
