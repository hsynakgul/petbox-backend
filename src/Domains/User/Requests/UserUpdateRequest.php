<?php


namespace Domains\User\Requests;


use Domains\User\Models\User;

class UserUpdateRequest extends UserStoreRequest
{
    public function rules(): array
    {
        $user = $this->route("user");
        return array_merge(parent::rules(), [
            'name' => 'required|max:255',
            'email' => 'required|email|max:255|unique:users,email,' . $user->id,
            'password' => 'nullable|min:6|max:16',
            'type' => 'required|in:' . implode(',', User::ENUM["types"]),
            'gender' => 'in:0,1,2',
            'send_email' => 'in:0,1',
            'send_sms' => 'in:0,1',
            'send_notification' => 'in:0,1',
        ]);
    }
}
