<?php


namespace Domains\User\Resources;


use Illuminate\Http\Resources\Json\JsonResource;

class UserResource extends JsonResource
{
    public function toArray($request)
    {
        return [
            "id" => $this->id,
            "type" => $this->type,
            "name" => $this->name,
            "gender" => $this->gender,
            "phone" => $this->phone,
            "phone_is_valid" => $this->phone_is_valid,
            "email" => $this->email,
            "email_is_valid" => $this->email_is_valid,
            "send_email" => $this->send_email,
            "send_notification" => $this->send_notification,
            "send_sms" => $this->send_sms,
            "status" => $this->status,
        ];
    }
}
