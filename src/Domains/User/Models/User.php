<?php


namespace Domains\User\Models;


use Domains\User\Observers\UserObserver;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Notifications\Notifiable;
use Laravel\Sanctum\HasApiTokens;

class User extends Authenticatable
{
    use HasFactory, Notifiable, HasApiTokens;

    public const TYPE_ADMIN = 1;
    public const TYPE_USER = 2;

    const ENUM = [
        "types" => [
            self::TYPE_ADMIN,
            self::TYPE_USER,
        ]
    ];

    protected $guarded = [
        'id',
        'email_validation_hash',
        'gsm_validation_hash',
        'remember_token',
        'remember_expiry',
    ];

    protected $hidden = [
        'password',
        'email_validation_hash',
        'gsm_validation_hash',
        'remember_token',
        'remember_expiry',
        'type'
    ];

    protected $casts = [
        'email_verified_at' => 'datetime',
        'phone_verified_at' => 'datetime',
    ];

    protected static function boot()
    {
        parent::boot();

        self::observe(UserObserver::class);
    }

    public function selectedTokens($device_name = null)
    {
        return $this->tokens()->when($device_name, function ($query) use ($device_name) {
            $query->where('name', $device_name);
        });
    }

    public function setNameAttribute($value)
    {
        if (is_string($value)) $value = ucwords_tr($value);
        $this->attributes['name'] = $value;
    }
}
