<?php


namespace Domains\User\Actions;


use Domains\User\Models\User;

class DeleteUserAction
{
    private array $success;
    private array $failed;

    public function __construct()
    {
        $this->success = [];
        $this->failed = [];
    }

    public function __invoke(array $ids): array
    {
        foreach ($ids as $id) $this->eachItem($id);
        return [$this->success, $this->failed];
    }

    private function eachItem($id)
    {
        $deleted = User::where("id", $id)->delete();
        if ($deleted === 0) {
            $this->failed[] = [
                "id" => $id,
                "message" => "Kullanıcı silinemedi! Böyle bir kullanıcı olmayabilir."
            ];
            return;
        }

        $this->success[] = $id;
    }
}
