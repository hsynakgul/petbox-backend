<?php


namespace Domains\User\Actions;


use Domains\User\Models\User;

class RestoreUserAction
{
    private array $success;
    private array $failed;

    public function __construct()
    {
        $this->success = [];
        $this->failed = [];
    }

    public function __invoke(array $ids): array
    {
        foreach ($ids as $id) $this->eachItem($id);
        return [$this->success, $this->failed];
    }

    private function eachItem($id)
    {
        $data = User::onlyTrashed()->where("id", $id)->select(["id"])->first();
        if (!$data) {
            $this->failed[] = [
                "id" => $id,
                "message" => "Kullanıcı bulunamadı! Böyle bir silinmiş kullanıcı olmayabilir."
            ];
            return;
        }
        $data->restore();
        $this->success[] = $id;
    }
}
