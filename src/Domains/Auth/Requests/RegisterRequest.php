<?php


namespace Domains\Auth\Requests;


use Illuminate\Foundation\Http\FormRequest;

class RegisterRequest extends FormRequest
{
    public function rules()
    {
        return [
            'email' => 'required|email|max:255|unique:users,email',
            'name' => 'required|max:255',
            'password' => 'required|min:6|max:16',
            'gender' => 'in:0,1,2',
            'send_email' => 'in:0,1',
            'send_sms' => 'in:0,1',
            'send_notification' => 'in:0,1',
            'device_name' => 'required',
        ];
    }
}
