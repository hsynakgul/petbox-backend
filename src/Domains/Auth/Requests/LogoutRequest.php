<?php


namespace Domains\Auth\Requests;


use Illuminate\Foundation\Http\FormRequest;

class LogoutRequest extends FormRequest
{
    public function rules()
    {
        return [
            'device_name' => 'required',
        ];
    }
}
