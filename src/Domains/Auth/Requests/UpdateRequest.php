<?php


namespace Domains\Auth\Requests;



class UpdateRequest extends RegisterRequest
{
    public function rules()
    {
        return array_merge(parent::rules(), [
            'email' => 'required|email|max:255|unique:users,email,' . auth()->id(),
            'password' => 'nullable|min:6|max:16',
            "device_name" => "nullable"
        ]);
    }
}
