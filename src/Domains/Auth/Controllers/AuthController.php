<?php


namespace Domains\Auth\Controllers;


use App\Http\Controllers\Controller;
use Domains\Auth\DataTransferObjects\AuthData;
use Domains\Auth\Requests\LoginRequest;
use Domains\Auth\Requests\LogoutRequest;
use Domains\Auth\Requests\RegisterRequest;
use Domains\Auth\Requests\UpdateRequest;
use Domains\Auth\Resources\AuthResource;
use Domains\User\Models\User;
use Illuminate\Http\Request;
use Pusher\Pusher;

class AuthController extends Controller
{
    public function me()
    {
        return new AuthResource(auth()->user());
    }

    public function login(LoginRequest $request)
    {
        $user = $request->user;
        return (new AuthResource($user))->additional([
            "meta" => [
                "token" => $user->createToken($request->device_name)->plainTextToken,
            ],
        ]);
    }

    public function logout(LogoutRequest $request)
    {
        /** @var User $user */
        $user = $request->user();
        $user->selectedTokens($request->device_name)->delete();
        return response()->noContent();
    }

    public function register(RegisterRequest $request)
    {
        $userData = AuthData::fromRequest($request);
        $user = User::create($userData->toArray());
        return (new AuthResource($user))->additional([
            "meta" => [
                "token" => $user->createToken($request->device_name)->plainTextToken,
            ],
        ]);
    }

    public function update(UpdateRequest $request)
    {
        /** @var User $user */
        $user = $request->user();
        $userData = AuthData::fromRequest($request);
        $user->fill($userData->toArray());
        $user->save();
        return new AuthResource($user->refresh());
    }

    public function suspend()
    {
        /** @var User $user */
        $user = auth()->user();
        $user->suspend = 1;
        $user->save();

        $user->tokens()->delete();
        return response()->noContent();
    }

    public function destroy()
    {
        /** @var User $user */
        $user = auth()->user();
        $user->tokens()->delete();
        $user->delete();
        return response()->noContent();
    }

    public function pusherAuth(Request $request)
    {
        $request->validate([
            "socket_id" => "required",
            "channel_name" => "required",
        ]);

        $pusherConfig = config("broadcasting.connections.pusher");
        $pusher = new Pusher($pusherConfig["key"], $pusherConfig["secret"], $pusherConfig["app_id"], $pusherConfig["options"]);

        $channelName = $request->get("channel_name");
        $myChannelName = "private-users-" . auth()->id();
        if ($channelName !== $myChannelName)
            abort(403, "Bu kanalı dinleyemezsiniz!");

        return $pusher->presence_auth(
            $channelName,
            $request->get("socket_id"),
            auth()->id()
        );
    }
}
