<?php


namespace Domains\Auth\DataTransferObjects;


use Domains\Auth\Requests\RegisterRequest;
use Domains\User\Models\User;
use Domains\User\Requests\UserStoreRequest;
use Illuminate\Support\Collection;
use Spatie\DataTransferObject\DataTransferObject;

class AuthData extends DataTransferObject
{
    public ?int $type;
    public ?string $name;
    public ?string $email;
    public ?string $phone;
    public ?int $gender;
    public ?int $status;
    public ?int $send_email;
    public ?int $send_sms;
    public ?int $send_notification;
    public ?string $password;

    public static function fromRequest(RegisterRequest $request): self
    {
        return self::fromCollection(collect($request));
    }

    public static function fromCollection(Collection $request): self
    {
        $instance = new self([
            "type" => User::TYPE_USER,
            "name" => $request->get("name", ""),
            "email" => $request->get("email", ""),
            "phone" => $request->get("phone", ""),
            "gender" => (int)$request->get("gender", 0),
            "status" => (int)$request->get("status", 1),
            "send_email" => (int)$request->get("send_email", 1),
            "send_sms" => (int)$request->get("send_email", 1),
            "send_notification" => (int)$request->get("send_notification", 1),
        ]);

        if ($request->get("password")) {
            $instance->password = \Hash::make($request->get("password"));
        } else {
            $instance = $instance->except("password");
        }

        return $instance;
    }
}
