<?php


namespace Domains\Auth\Resources;


use Illuminate\Http\Resources\Json\JsonResource;

class AuthResource extends JsonResource
{
    public function toArray($request)
    {
        return [
            "id" => (int)$this->id,
            "name" => (string)$this->name,
            "email" => (string)$this->email,
            "phone" => (string)$this->phone,
            "gender" => (int)$this->gender,
            "send_email" => (int)$this->send_email,
            "send_sms" => (int)$this->send_sms,
            "send_notification" => (int)$this->send_notification,
        ];
    }
}
