<?php


namespace Domains\Auth\Helpers;


use Illuminate\Auth\GuardHelpers;
use Illuminate\Contracts\Auth\Guard;

class DummyGuard implements Guard
{
    use GuardHelpers;

    public function user()
    {
        return $this->user;
    }

    public function validate(array $credentials = [])
    {
        return false;
    }
}
