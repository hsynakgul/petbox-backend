<?php


namespace Domains\Chat\Models;


use Domains\User\Models\User;
use Illuminate\Database\Eloquent\Model;

class Chat extends Model
{
    protected $visible = ['id'];

    public function lastMessage()
    {
        return $this->hasOne(ChatMessage::class, "chat_id", "id")->orderBy("id", "desc");
    }

    public function messages()
    {
        return $this->hasMany(ChatMessage::class, "chat_id", "id")->orderBy("id", "desc");
    }

    public function users()
    {
        return $this->belongsToMany(
            User::class,
            'chat_user',
            'chat_id',
            'user_id',
            'id',
            'id'
        );
    }
}
