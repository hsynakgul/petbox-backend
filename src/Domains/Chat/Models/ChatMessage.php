<?php


namespace Domains\Chat\Models;


use Domains\Chat\Observers\ChatMessageObserver;
use Illuminate\Database\Eloquent\Model;

class ChatMessage extends Model
{
    protected $visible = ['id', 'text'];
    protected $guarded = ["id"];
    protected $fillable = ['chat_id', 'user_id', 'text'];

    protected static function boot()
    {
        parent::boot();

        self::observe(ChatMessageObserver::class);
    }

    public function chat()
    {
        return $this->belongsTo(Chat::class);
    }
}
