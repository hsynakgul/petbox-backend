<?php


namespace Domains\Chat\Requests;


use Domains\User\Models\User;
use Illuminate\Foundation\Http\FormRequest;

class ChatStoreRequest extends FormRequest
{
    public function rules()
    {
        return [
            "from_users" => "required|array|min:1"
        ];
    }

    protected function passedValidation()
    {
        $fromUsers = collect($this->get("from_users", []));
        $users = User::whereIn("id", $fromUsers)->get(["id"]);
        $fromUsers->map(function ($userId) use ($users) {
            if ($users->where("id", $userId)->count() === 0)
                abort(500, "$userId numaralı bir kullanıcı yok!");
        });
    }

}
