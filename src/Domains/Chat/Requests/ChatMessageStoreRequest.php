<?php


namespace Domains\Chat\Requests;


use Domains\Chat\Models\Chat;
use Illuminate\Foundation\Http\FormRequest;

class ChatMessageStoreRequest extends FormRequest
{
    public Chat $chat;

    public function rules()
    {
        return [
            "chat_id" => "required|int",
            "text" => "required",
        ];
    }

    protected function passedValidation()
    {
        $this->chat = Chat::findOrFail($this->get("chat_id"))->load("users");
        if ($this->chat->users->where("id", auth()->id())->count() === 0)
            abort(403, "Bu sohbete mesaj gönderemezsiniz!");
    }
}
