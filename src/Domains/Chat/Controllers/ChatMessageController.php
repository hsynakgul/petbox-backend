<?php


namespace Domains\Chat\Controllers;


use App\Http\Controllers\Controller;
use Domains\Chat\Models\ChatMessage;
use Domains\Chat\Requests\ChatMessageStoreRequest;
use Domains\Chat\Resources\ChatMessageResource;
use Domains\Post\Models\Post;
use Illuminate\Http\Request;

class ChatMessageController extends Controller
{
    public function index(Request $request)
    {

    }

    public function store(ChatMessageStoreRequest $request)
    {
        /** @var ChatMessage $message */
        $message = ChatMessage::create([
            "chat_id" => $request->chat->id,
            "user_id" => auth()->id(),
            "text" => $request->get("text", "")
        ]);

        $message->setRelation("chat", $request->chat);

        return new ChatMessageResource($message);
    }

    public function destroy(Post $post)
    {

    }
}
