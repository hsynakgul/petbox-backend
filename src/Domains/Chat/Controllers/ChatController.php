<?php


namespace Domains\Chat\Controllers;


use App\Http\Controllers\Controller;
use Domains\Chat\Models\Chat;
use Domains\Chat\Requests\ChatStoreRequest;
use Domains\Chat\Resources\ChatResource;
use Illuminate\Http\Request;

class ChatController extends Controller
{
    public function index(Request $request)
    {
        $chats = Chat::with(["users" => function($query) {
            $query->where("user_id", "!=", auth()->id());
        }, "messages"])
            ->whereHas("users", function ($query) {
                $query->where("user_id", auth()->id());
            })->orderBy("id", "desc")->get();

        return ChatResource::collection($chats);
    }

    public function store(ChatStoreRequest $request)
    {
        /** @var Chat $chat */
        $chat = Chat::create();
        $chat->users()->sync(array_merge([auth()->id()], $request->get("from_users")));
        $chat->load(["users", "messages"]);
        return new ChatResource($chat);
    }

    public function destroy(Chat $chat)
    {

    }
}
