<?php


namespace Domains\Chat\Resources;


use Illuminate\Http\Resources\Json\JsonResource;

class ChatUserResource extends JsonResource
{
    public function toArray($request)
    {
        return [
            "id" => (int)$this->id,
            "name" => $this->name
        ];
    }
}
