<?php


namespace Domains\Chat\Resources;


use Illuminate\Http\Resources\Json\JsonResource;

class ChatMessageResource extends JsonResource
{
    public function toArray($request)
    {
        return [
            "id" => (int)$this->id,
            "chat_id" => (int)$this->chat_id,
            "user_id" => (int)$this->user_id,
            "text" => (string)$this->text,
            "created_at" => $this->created_at,
            "created_at_text" => dateFormat($this->created_at, "date-long"),
        ];
    }
}
