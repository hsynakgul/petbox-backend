<?php


namespace Domains\Chat\Resources;


use Illuminate\Http\Resources\Json\JsonResource;

class ChatResource extends JsonResource
{
    public function toArray($request)
    {
        return [
            "id" => (int)$this->id,
            "users" => ChatUserResource::collection($this->whenLoaded("users")),
            "messages" => ChatMessageResource::collection($this->whenLoaded("messages"))
//            "messages" => [
//                new ChatMessageResource($this->whenLoaded("lastMessage"))
//            ]
        ];
    }
}
