<?php


namespace Domains\Chat\Events;


use Domains\Chat\Models\ChatMessage;
use Domains\Chat\Resources\ChatMessageResource;
use Illuminate\Broadcasting\Channel;
use Illuminate\Queue\SerializesModels;
use Illuminate\Foundation\Events\Dispatchable;
use Illuminate\Broadcasting\InteractsWithSockets;
use Illuminate\Contracts\Broadcasting\ShouldBroadcast;

class ChatMessageCreatedEvent implements ShouldBroadcast
{
    use Dispatchable, InteractsWithSockets, SerializesModels;

    public $message;
    private $chatMessage;

    public function __construct(ChatMessage $chatMessage)
    {
        $this->chatMessage = $chatMessage;
        $resource = new ChatMessageResource($chatMessage);
        $this->message = base64_encode($resource->toJson());
    }

    public function broadcastOn()
    {
        $channels = [];
        foreach ($this->chatMessage->chat->users as $user) {
            if ($user->id === auth()->id()) continue;
            $channels[] = new Channel('private-users-' . $user->id);
        }
        return $channels;
    }

    public function broadcastAs()
    {
        return "send-message";
    }
}
