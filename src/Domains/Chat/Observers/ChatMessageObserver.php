<?php


namespace Domains\Chat\Observers;

use Domains\Chat\Events\ChatMessageCreatedEvent;
use Domains\Chat\Models\ChatMessage;

class ChatMessageObserver
{
    public function created(ChatMessage $chatMessage)
    {
        broadcast(new ChatMessageCreatedEvent($chatMessage));
    }
}
