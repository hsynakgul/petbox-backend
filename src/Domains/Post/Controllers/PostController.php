<?php


namespace Domains\Post\Controllers;


use App\Http\Controllers\Controller;
use App\Http\Requests\GlobalBatchDestroyRequest;
use Domains\Post\Actions\DeletePostAction;
use Domains\Post\DataTransferObjects\PostData;
use Domains\Post\Models\Post;
use Domains\Post\Requests\PostStoreRequest;
use Domains\Post\Resources\PostResource;
use Illuminate\Http\Request;

class PostController extends Controller
{
    public function index(Request $request)
    {
        $post = Post::with(["category", "breed", "photos", "user"]);

        if ($request->has("category_id"))
            $post->where("category_id", $request->get("category_id"));
        if ($request->has("user_id"))
            $post->where("user_id", $request->get("user_id"));
        if (
            $request->get("get_passive", false) === false
            || (int)$request->get("user_id", 0) !== auth()->id()
        )
            $post->where("status", 1);

        $post->orderBy("id", "desc");
        return PostResource::collection($post->paginate());
    }

    public function show(Post $post)
    {
        if ($post->status === 0 && $post->user_id !== auth()->id()) {
            abort(403, "Bu ilan aktif değil!");
        }
        $post->load(["category", "breed", "photos", "user"])
            ->loadCount(["favorite" => function ($query) {
                $query->where('user_id', auth()->id());
            }]);
        return new PostResource($post);
    }

    public function store(
        PostStoreRequest $request
    )
    {
        $postData = PostData::fromRequest($request);
        $post = Post::create($postData->except("photos")->toArray());
        $post->photos()->sync($postData->photos);
        return new PostResource(
            $post->load(["category", "breed", "photos", "user"])
        );
    }

    public function update(
        PostStoreRequest $request,
        Post $post
    )
    {
        if ($post->user_id !== auth()->id()) {
            abort(403, "Bu ilan size ait değil!");
        }
        $postData = PostData::fromRequest($request);
        $post->fill($postData->except("user_id", "photos")->toArray())
            ->save();
        $post->photos()->sync($postData->photos);
        return new PostResource(
            $post->refresh()->load(["category", "breed", "photos", "user"])
        );
    }

    public function destroy(Post $post)
    {
        if ($post->user_id !== auth()->id()) {
            abort(403, "Bu ilan size ait değil!");
        }

        $post->photos()->sync([]);
        $post->favorite()->sync([]);
        $post->delete();

        return response()->noContent();
    }

    public function batchDestroy(
        GlobalBatchDestroyRequest $request,
        DeletePostAction $deletePostAction,
    )
    {
        list($success, $failed) = $deletePostAction($request->get("id", []));
        return response()->json(compact('success', 'failed'));
    }

    public function status(
        Post $post,
        Request $request
    )
    {
        if ($post->user_id !== auth()->id()) {
            abort(403, "Bu ilan size ait değil!");
        }

        $post->favorite()->sync([]);
        $post->status = $request->input("status", 0);
        $post->save();

        return response()->noContent();
    }

    public function favorite(
        Post $post,
        Request $request
    )
    {
        if ($post->status === 0) {
            abort(403, "Bu ilan aktif değil!");
        }

        if ($post->user_id === auth()->id()) {
            abort(500, "Kendi ilanınızı favorilere ekleyemezsiniz!");
        }

        $post->favorite()->detach(auth()->id());

        if ($request->input("value", 0) * 1 === 1)
            $post->favorite()->attach(auth()->id());

        return response()->noContent();
    }

    public function favorites()
    {
        $post = Post::where("status", 1)
            ->with(["category", "breed", "photos", "user"])
            ->whereHas("favorite", function ($query) {
                $query->where("user_id", auth()->id());
            })
            ->orderBy("id", "desc");
        return PostResource::collection($post->get());
    }
}
