<?php


namespace Domains\Post\Requests;


use Illuminate\Foundation\Http\FormRequest;

class PostStoreRequest extends FormRequest
{
    public function rules(): array
    {
        return [
            "title" => "required|max:255",
            "description" => "required",
            "category_id" => "required|exists:categories,id",
            "breed_id" => "required|exists:breeds,id",
            "sex" => "required|in:0,1,2",
            "age" => "required|integer",
            "city" => "required",
            "district" => "required",
            "photos" => "required|array|min:1",
            "photos.*.id" => "required",
        ];
    }
}
