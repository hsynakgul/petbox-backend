<?php


namespace Domains\Post\Resources;


use Domains\Breed\Resources\BreedResource;
use Domains\Category\Resources\CategoryResource;
use Domains\Chat\Resources\ChatUserResource;
use Domains\File\Resources\FileResource;
use Illuminate\Http\Resources\Json\JsonResource;

class PostResource extends JsonResource
{
    public function toArray($request)
    {
        return [
            "id" => (int)$this->id,
            "user_id" => (int)$this->user_id,
            "title" => $this->title,
            "description" => $this->description,
            "sex" => (int)$this->sex,
            "age" => (int)$this->age,
            "city" => $this->city,
            "district" => $this->district,
            "status" => (int)$this->status,
            "favorite" => $this->favorite_count > 0 ? 1 : 0,
            "created_at" => dateFormat($this->created_at, "date-long"),
            "category_id" => (int)$this->category_id,
            "breed_id" => (int)$this->breed_id,
            "category" => new CategoryResource($this->whenLoaded("category")),
            "breed" => new BreedResource($this->whenLoaded("breed")),
            "user" => new ChatUserResource($this->whenLoaded("user")),
            "photos" => FileResource::collection($this->whenLoaded("photos")),
        ];
    }
}
