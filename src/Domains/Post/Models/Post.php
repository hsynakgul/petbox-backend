<?php


namespace Domains\Post\Models;


use Domains\Breed\Models\Breed;
use Domains\Category\Models\Category;
use Domains\File\Models\File;
use Domains\User\Models\User;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsToMany;
use Illuminate\Database\Eloquent\Relations\HasOne;

class Post extends Model
{
    protected $guarded = ['id'];
    protected $casts = ["created_at" => "string"];

    public function user(): HasOne
    {
        return $this->hasOne(User::class, "id", "user_id");
    }

    public function category(): HasOne
    {
        return $this->hasOne(Category::class, "id", "category_id");
    }

    public function breed(): HasOne
    {
        return $this->hasOne(Breed::class, "id", "breed_id");
    }

    public function photos(): BelongsToMany
    {
        return $this->belongsToMany(
            File::class,
            'content_file',
            'relation_id',
            'file_id',
            'id',
            'id'
        )
            ->withPivotValue('relation_type', Post::class)
            ->withPivotValue('type', 1)
            ->withPivot(['sq', 'is_cover'])
            ->orderBy('is_cover', 'desc')
            ->orderBy('sq');
    }

    public function favorite(): BelongsToMany
    {
        return $this->belongsToMany(
            User::class,
            'post_favorite',
            'post_id',
            'user_id',
            'id',
            'id'
        );
    }
}
