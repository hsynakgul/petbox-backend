<?php


namespace Domains\Post\Actions;


use Domains\Post\Models\Post;

class DeletePostAction
{
    private array $success;
    private array $failed;

    public function __construct()
    {
        $this->success = [];
        $this->failed = [];
    }

    public function __invoke(array $ids): array
    {
        foreach ($ids as $id) $this->eachItem($id);
        return [$this->success, $this->failed];
    }

    private function eachItem($id)
    {
        $deleted = Post::where("id", $id)->delete();
        if ($deleted === 0) {
            $this->failed[] = [
                "id" => $id,
                "message" => "Paylaşım silinemedi! Böyle bir paylaşım olmayabilir."
            ];
            return;
        }

        $this->success[] = $id;
    }
}
