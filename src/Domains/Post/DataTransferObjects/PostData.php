<?php


namespace Domains\Post\DataTransferObjects;


use Domains\Post\Requests\PostStoreRequest;
use Illuminate\Support\Collection;
use Spatie\DataTransferObject\DataTransferObject;

class PostData extends DataTransferObject
{
    public ?string $title;
    public ?string $description;
    public ?int $category_id;
    public ?int $breed_id;
    public ?int $user_id;
    public ?int $sex;
    public ?string $age;
    public ?string $city;
    public ?string $district;
    public ?int $status;
    public ?array $photos;

    public static function fromRequest(PostStoreRequest $request): self
    {
        return self::fromCollection(collect($request));
    }

    public static function fromCollection(Collection $request): self
    {
        $photos = collect($request->get("photos", []));
        return new self([
            "user_id" => auth()->id(),
            'title' => $request->get("title", ""),
            'description' => $request->get("description", ""),
            'category_id' => (int)$request->get("category_id", 0),
            'breed_id' => (int)$request->get("breed_id", 0),
            'sex' => (int)$request->get("sex", 0),
            'age' => (string)$request->get("age", ""),
            'city' => (string)$request->get("city", ""),
            'district' => (string)$request->get("district", ""),
            'status' => (int)$request->get("status", 1),
            'photos' => $photos->map(fn($item) => $item["id"] ?? null)->toArray(),
        ]);
    }
}
