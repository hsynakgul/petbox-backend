<?php


namespace Domains\Category\DataTransferObjects;


use Domains\Category\Requests\CategoryStoreRequest;
use Illuminate\Support\Collection;
use Spatie\DataTransferObject\DataTransferObject;

class CategoryData extends DataTransferObject
{
    public ?string $title_tr;
    public ?string $title_en;
    public ?int $parent_id;
    public ?int $status;
    public ?int $sq;

    public static function fromRequest(CategoryStoreRequest $request): self
    {
        return self::fromCollection(collect($request));
    }

    public static function fromCollection(Collection $request): self
    {
        return new self([
            'title_tr' => $request->get("title_tr", ""),
            'title_en' => $request->get("title_en", ""),
            'status' => (int)$request->get("status", 1),
            'sq' => (int)$request->get("sq", 0),
            'parent_id' => $request->get("parent_id"),
        ]);
    }
}
