<?php


namespace Domains\Category\Models;


use Domains\Breed\Models\Breed;
use Domains\Category\Observers\CategoryObserver;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\HasMany;
use Illuminate\Database\Eloquent\SoftDeletes;

class Category extends Model
{
    use SoftDeletes;

    protected $guarded = ['id', 'deleted_at'];

    protected static function boot()
    {
        parent::boot();

        self::observe(CategoryObserver::class);
    }

    public function breeds(): HasMany
    {
        return $this->hasMany(Breed::class, "category_id", "id");
    }

    public function getTitleAttribute($locale = null): string
    {
        if (!$locale) $locale = config('app.locale');
        $fallbackLocale = config('app.fallback_locale');

        if (array_key_exists("title", $this->attributes)) return $this->attributes["title"];
        if (array_key_exists("title_" . $locale, $this->attributes)) return $this->attributes["title_" . $locale];
        if (array_key_exists("title_" . $fallbackLocale, $this->attributes)) return $this->attributes["title_" . $fallbackLocale];
        return '';
    }

}
