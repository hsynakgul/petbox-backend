<?php


namespace Domains\Category\Observers;


use Domains\Category\Models\Category;

class CategoryObserver
{
    public function created(Category $category)
    {
        $category->sq = $category->id;
        $category->save();
    }
}
