<?php


namespace Domains\Category\Resources;


use Domains\Breed\Resources\BreedResource;
use Illuminate\Http\Resources\Json\JsonResource;

class CategoryResource extends JsonResource
{
    public function toArray($request)
    {
        return [
            "id" => $this->id,
            "sq" => $this->sq,
            "title" => $this->title,
            "breeds" => BreedResource::collection($this->whenLoaded("breeds"))
        ];
    }
}
