<?php


namespace Domains\Category\Requests;


use Illuminate\Foundation\Http\FormRequest;

class CategoryStoreRequest extends FormRequest
{
    public function rules(): array
    {
        return [
            "sq" => "nullable|integer",
            "title_tr" => "required|max:255",
            "title_en" => "nullable|max:255",
            "status" => "nullable|integer|in:0,1",
            "parent_id" => "nullable|exists:categories,id",
        ];
    }

    public function attributes(): array
    {
        return [
            "title_tr" => "Başlık",
            "title_en" => "İnglizce Başlık",
            "status" => "Durum",
            "parent_id" => "Üst kategori",
        ];
    }
}
