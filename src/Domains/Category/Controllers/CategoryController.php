<?php


namespace Domains\Category\Controllers;


use App\Http\Controllers\Controller;
use App\Http\Requests\GlobalBatchDestroyRequest;
use Domains\Category\Actions\DeleteCategoryAction;
use Domains\Category\Actions\RestoreCategoryAction;
use Domains\Category\DataTransferObjects\CategoryData;
use Domains\Category\Models\Category;
use Domains\Category\Requests\CategoryStoreRequest;
use Domains\Category\Resources\CategoryResource;

class CategoryController extends Controller
{
    public function index()
    {
        return CategoryResource::collection(Category::where("status", 1)->with("breeds")->get());
    }

    public function show(Category $category)
    {
        return new CategoryResource($category);
    }

    public function store(CategoryStoreRequest $request)
    {
        $categoryData = CategoryData::fromRequest($request);
        return new CategoryResource(
            Category::create($categoryData->toArray())
        );
    }

    public function update(CategoryStoreRequest $request, Category $category)
    {
        $categoryData = CategoryData::fromRequest($request);
        $category->fill($categoryData->toArray())
            ->save();
        return new CategoryResource(
            $category->refresh()
        );
    }

    public function destroy(Category $category)
    {
        return $category->delete();
    }

    public function batchDestroy(GlobalBatchDestroyRequest $request, DeleteCategoryAction $deleteCategoryAction)
    {
        list($success, $failed) = $deleteCategoryAction($request->get("id", []));
        return response()->json(compact('success', 'failed'));
    }

    public function batchRestore(GlobalBatchDestroyRequest $request, RestoreCategoryAction $restoreCategoryAction)
    {
        list($success, $failed) = $restoreCategoryAction($request->get("id", []));
        return response()->json(compact('success', 'failed'));
    }
}
