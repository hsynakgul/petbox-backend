<?php


namespace Domains\Category\Actions;


use Domains\Category\Models\Category;

class RestoreCategoryAction
{
    private array $success;
    private array $failed;

    public function __construct()
    {
        $this->success = [];
        $this->failed = [];
    }

    public function __invoke(array $ids): array
    {
        foreach ($ids as $id) $this->eachItem($id);
        return [$this->success, $this->failed];
    }

    private function eachItem($id)
    {
        $data = Category::onlyTrashed()->where("id", $id)->select(["id", "deleted_at"])->first();
        if (!$data) {
            $this->failed[] = [
                "id" => $id,
                "message" => "Kategori bulunamadı! Böyle bir silinmiş kategori olmayabilir."
            ];
            return;
        }
        $data->restore();
        $this->success[] = $id;
    }
}
