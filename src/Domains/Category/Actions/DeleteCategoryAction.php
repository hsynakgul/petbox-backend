<?php


namespace Domains\Category\Actions;


use Domains\Category\Models\Category;

class DeleteCategoryAction
{
    private array $success;
    private array $failed;

    public function __construct()
    {
        $this->success = [];
        $this->failed = [];
    }

    public function __invoke(array $ids): array
    {
        foreach ($ids as $id) $this->eachItem($id);
        return [$this->success, $this->failed];
    }

    private function eachItem($id)
    {
        $deleted = Category::where("id", $id)->delete();
        if ($deleted === 0) {
            $this->failed[] = [
                "id" => $id,
                "message" => "Kategori silinemedi! Böyle bir kategori olmayabilir."
            ];
            return;
        }

        $this->success[] = $id;
    }
}
