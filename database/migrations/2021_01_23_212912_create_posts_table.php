<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreatePostsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('posts', function (Blueprint $table) {
            $table->id();
            $table->string("title", 255);
            $table->integer("user_id")->unsigned();
            $table->integer("category_id")->unsigned();
            $table->string("breed_id")->nullable();
            $table->integer("sex")->unsigned()->nullable();
            $table->integer("age")->unsigned()->nullable();
            $table->string("city")->nullable();
            $table->string("district")->nullable();
            $table->text("description")->nullable();
            $table->tinyInteger("status")->default(0)->unsigned();
            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('posts');
    }
}
