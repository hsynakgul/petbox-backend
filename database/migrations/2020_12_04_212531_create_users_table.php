<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateUsersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('users', function (Blueprint $table) {
            $table->id();
            $table->tinyInteger("type")->default(\Domains\User\Models\User::TYPE_USER)->unsigned();
            $table->string("name");
            $table->string("email");
            $table->dateTime("email_verified_at")->nullable();
            $table->string("email_validation_hash")->nullable();
            $table->string("phone", 25)->nullable();
            $table->dateTime("phone_verified_at")->nullable();
            $table->string("phone_validation_hash")->nullable();
            $table->tinyInteger("gender")->default(0)->unsigned();
            $table->string("password");
            $table->string("remember_token")->nullable();
            $table->dateTime("remember_expiry")->nullable();
            $table->tinyInteger("send_email")->default(0)->unsigned();
            $table->tinyInteger("send_sms")->default(0)->unsigned();
            $table->tinyInteger("send_notification")->default(0)->unsigned();
            $table->tinyInteger("status")->default(1)->unsigned();
            $table->tinyInteger("suspend")->default(0)->unsigned();
            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('users');
    }
}
