<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateContentFileTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('content_file', function (Blueprint $table) {
            $table->id();
            $table->integer('sq')->nullable();
            $table->boolean('type')->nullable()->default(1);
            $table->integer('relation_id')->nullable();
            $table->string('relation_type')->nullable();
            $table->integer('file_id')->nullable();
            $table->tinyInteger('is_cover')->nullable()->default(1);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('content_file');
    }
}
